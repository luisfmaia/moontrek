package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

/**
 * Created by luisfmss on 01/04/15.
 */
public class ShakeAction extends TemporalAction {
    private float jitter = 0;

    public void setJitter(float jitter) {
        this.jitter = jitter;
    }

    @Override
    protected void update(float percent) {
        if (((percent * 100) % 2) > 0) {

            target.setPosition(target.getX() - jitter, target.getY());
        } else {
            target.setPosition(target.getX() + jitter, target.getY());
        }
    }
}
