package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.TimeUtils;


/**
 * Created by luisfmss on 23/03/15.
 */
public class SpaceShip implements CollisionObject, Disposable {
    private SpriteBatch batch;
    private Texture ship, shipOn;
    private TexturedObject bubbleShield;
    private Sprite sprite;

    float moveSpeed;
    int fallSpeed;
    Vector2 position;
    Vector2 center;

    boolean moveRight = true;//true RIGHT, false LEFT
    final int xLowLimit = 30;
    final int xHighLimit = 880;
    final int yLowLimit = 50;
    final int yHighLimit = 400;

    private float rad;
    private long immuneTimer;
    private boolean isImmune;
    private boolean isShielded;
    private final int IMMUNE_TIME = 2000;
    private long pauseTime;

    public SpaceShip(SpriteBatch batch) {
        this.batch = batch;

        ship = new Texture("spaceship.png");
        shipOn = new Texture("spaceship-on.png");
        sprite = new Sprite(ship);

        bubbleShield = new TexturedObject(batch, "bubble.png");

        immuneTimer = 0;
        isImmune = false;
        isShielded = false;
        position = new Vector2();
        center = new Vector2();

        restart();
        setRadiusMin();
    }

    public void restart() {
        moveRight = true;
        position.x = 0;
        position.y = 450;
        setRotation(-90);
        moveSpeed = 100.0f;
        fallSpeed = -50;
        pauseTime = 0;
    }

    public void draw() {
        sprite.draw(batch);

        if (isShielded) {

            if (!bubbleShield.isTimeExpired()) {
                bubbleShield.draw();
            } else {
                isShielded = false;
            }
        }

        if (isImmune) {

            long timeDiff = getElapsedTime();
            if (timeDiff < IMMUNE_TIME) {
                sprite.setAlpha((timeDiff / 200) % 2);
            } else {
                sprite.setAlpha(1);
                isImmune = false;
            }
        }

    }

    void updatePosition(float delta) {
        if (pauseTime > 0) return;

        if (position.x < xLowLimit) {
            setRotation(-90);
            moveRight = true;//move right
        } else if (position.x > xHighLimit) {
            setRotation(90);
            moveRight = false;//move left
        }

//        if (position.x < xLowLimit) {
//            position.x = xLowLimit;//game over
//        } else if (position.x > xHighLimit) {
//            position.x = xHighLimit;
//        }

        if (position.y < yLowLimit) {
            position.y = yLowLimit;//game over
        } else if (position.y > yHighLimit) {
            position.y = yHighLimit;
        }

        if (moveRight) {
            position.x += moveSpeed * delta;
        } else {
            position.x -= moveSpeed * delta;
        }
        position.y += fallSpeed * delta;

        setPosition(position);
    }

    public boolean collides(CollisionObject obj) {

        Vector2 center = getCenter();
        Vector2 objCenter = obj.getCenter();
        if (center.dst2(objCenter) < obj.radius() * this.radius())
            return true;
        else
            return false;
    }

    private long getElapsedTime() {
        if (pauseTime > 0) {
            return (pauseTime - immuneTimer);
        } else {
            return (TimeUtils.millis() - immuneTimer);
        }
    }

    public void pause() {
        pauseTime = TimeUtils.millis();
        bubbleShield.pause();
    }

    public void resume() {
        immuneTimer += TimeUtils.millis() - pauseTime;
        pauseTime = 0;
        bubbleShield.resume();
    }

    public void setLoseLife() {
        if (!isImmune) {
            isImmune = true;
            immuneTimer = TimeUtils.millis();
        }
    }

    public boolean isShieldedOrImmune() {
        if (isImmune || isShielded) {
            return true;
        }
        return false;
    }

    public void setBubbleShieldOn() {
//        if (bubbleShield != null) {
//            bubbleShield.dispose();
//            bubbleShield = null;
//        }
        isShielded = true;
        bubbleShield.getSprite().setAlpha(0.5f);
        bubbleShield.restart();
    }

    public void setRadiusMax() {
        rad = Math.max(sprite.getWidth(), sprite.getHeight());
    }

    public void setRadiusMin() {
        rad = Math.min(sprite.getWidth(), sprite.getHeight());
    }

    public float radius() {
        return rad;
    }

    public void setRotation(int r) {
        sprite.setRotation(r);
    }

    public Vector2 getCenter() {
        return center;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        sprite.setPosition(position.x, position.y);
        if (bubbleShield != null) {
            bubbleShield.setPosition(position.x - 35, position.y - 10);
        }
        sprite.getBoundingRectangle().getCenter(center);
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
        if (bubbleShield != null) {
            bubbleShield.setPosition(x - 35, y - 10);
        }
        sprite.getBoundingRectangle().getCenter(center);
    }

    public void setEngineOn() {
        sprite.setTexture(shipOn);
        moveSpeed = 250f;
        fallSpeed = 100;
    }

    public void setEngineOff() {
        sprite.setTexture(ship);
        moveSpeed = 100f;
        fallSpeed = -50;
    }

    public void dispose() {
//        sprite = null;
        ship.dispose();
//        ship = null;
        shipOn.dispose();
//        shipOn = null;
        bubbleShield.dispose();
    }

    public void setLevel(float level) {
        moveSpeed *= level;
        fallSpeed *= level;
    }
}
