package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ParallelAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by luisfmss on 30/03/15.
 */
public class MenuScreen implements Screen {
    Texture background;
    SpriteBatch batch;
    MoonTrek game;
    Texture play;
    Skin skin;
    TexturedActor shipActor;
    Stage stage;
    long playTouched;
    BitmapFont font;
    String score;
//    private Music backgroundMenu;

    final int ANIM_TIME = 2;


    MenuScreen(MoonTrek game) {
        this.game = game;
    }

    @Override
    public void show() {
        playTouched = 0;

//        backgroundMenu = Gdx.audio.newMusic(Gdx.files.internal(("sounds/background-menu.mp3")));
//        backgroundMenu.setLooping(true);
//        backgroundMenu.setVolume(0.7f);
//        backgroundMenu.play();

        batch = new SpriteBatch();
        background = new Texture("background-menu.jpg");
        skin = new Skin();
        play = new Texture("play_button.png");

        initFont();

        stage = new Stage();
        shipActor = new TexturedActor("spaceship.png");
        shipActor.setPosition(250, 130);
        shipActor.setRotation(-90);
        shipActor.setScale(1.25f);


        ParallelAction parallelAction = new ParallelAction();

        RotateToAction rotate = new RotateToAction();
        rotate.setRotation(-30f);
        rotate.setDuration(ANIM_TIME);

        ScaleToAction scale = new ScaleToAction();
        scale.setScale(5f);
        scale.setDuration(ANIM_TIME);


        MoveToAction moveAction = new MoveToAction();
        moveAction.setPosition(1500f, 400f);
        moveAction.setDuration(ANIM_TIME);

        parallelAction.addAction(rotate);
        parallelAction.addAction(moveAction);
        parallelAction.addAction(scale);

        shipActor.addAction(parallelAction);

        stage.addActor(shipActor);

        score = "Record " + getScore();
    }

    @Override
    public void render(float delta) {


        batch.begin();
        batch.draw(background, 0, 0);
        batch.draw(play, 20, 100);
        font.draw(batch, score, 30, 290);
        batch.end();

        if (playTouched != 0) {
            stage.act(delta);

            if ((TimeUtils.millis() - playTouched) > ANIM_TIME * 1000) {
//                sound.pauseEngine();
//                backgroundMenu.stop();
//                backgroundMenu.dispose();
                game.setScreen(new GameScreen(game));
            }

        }
        stage.draw();

        if (Gdx.input.justTouched() && (playTouched == 0)) {

            if ((Gdx.input.getX() > 20) && (Gdx.input.getX() < 370) && (Gdx.input.getY() < 440) && (Gdx.input.getY() > 290)) {
                playTouched = TimeUtils.millis();
                shipActor.setTexture("spaceship-on.png");
            }
        }
    }

    private void initFont() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/exotic.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 45;
        parameter.borderColor = Color.valueOf("aa8800");
        parameter.color = Color.valueOf("751616");
        parameter.borderWidth = 3;
        font = generator.generateFont(parameter);
        generator.dispose();
    }

    private String getScore() {
        int scoreRecord = 0;
        Preferences prefs = Gdx.app.getPreferences("MoonTrek");
        if (prefs.contains("record")) {
            scoreRecord = prefs.getInteger("record");
        }

        return String.valueOf(Math.min(scoreRecord, 9999));
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        background.dispose();
    }

    @Override
    public void dispose() {

    }
}
