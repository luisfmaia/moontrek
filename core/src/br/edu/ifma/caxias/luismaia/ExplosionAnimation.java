package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by luisfmss on 26/03/15.
 */
public class ExplosionAnimation implements Disposable {
    private static final int FRAME_COLS = 4;
    private static final int FRAME_ROWS = 4;

    Animation<TextureRegion> animation;
    Texture sheet;
    TextureRegion[] frames;
    TextureRegion currentFrame;
    Vector2 position;
    SpriteBatch batch;

    int width, height;
    float stateTime;
    boolean isPaused;

    ExplosionAnimation(SpriteBatch spriteBatch) {
        batch = spriteBatch;

        sheet = new Texture("explosion.png");

        width = sheet.getWidth() / FRAME_COLS;
        height = sheet.getHeight() / FRAME_ROWS;

        TextureRegion[][] tmp = TextureRegion.split(sheet, width, height);
        frames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                frames[index++] = tmp[i][j];
            }
        }
        animation = new Animation(0.05f, frames);      // #11
        stateTime = 0f;
        position = new Vector2();
        isPaused = false;
    }

    void draw() {
        if (!isPaused) {
            stateTime += Gdx.graphics.getDeltaTime();
        }

        if (!animation.isAnimationFinished(stateTime)) {
            currentFrame = animation.getKeyFrame(stateTime, false);
            batch.draw(currentFrame, position.x, position.y);
        }
    }

    void setPosition(Vector2 pos) {
        position.x = pos.x - width * 0.5f;
        position.y = pos.y - height * 0.5f;
        stateTime = 0;
    }

    public void pause() {
        isPaused = true;
    }

    public void resume() {
        isPaused = false;
    }

    public void dispose() {
        sheet.dispose();
    }

}
