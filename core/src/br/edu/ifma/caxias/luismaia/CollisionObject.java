package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by luisfmss on 23/03/15.
 */
public interface CollisionObject {

    public Vector2 getPosition();

    public float radius();

    public Vector2 getCenter();

    public boolean collides(CollisionObject obj);

}
