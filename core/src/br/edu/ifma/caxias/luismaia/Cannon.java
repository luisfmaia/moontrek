package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by luisfmss on 28/03/15.
 */
public class Cannon implements Disposable {
    SpriteBatch batch;
    SoundPlayer sound;

    Texture cannon, cannonFire, cannonBase, beam;
    Sprite cannonSprite, beamSprite;

    Vector2 beamPosition;
    Vector2 beamAim;
    int bulletSpeed = 300;

    int cannonSpeed = 100;
    int cannonRotation;
    int cannonOrientation;//1 LEFT, -1 RIGHT

    long fireStartTime;
    long pauseTime;

    final int screenWidth = 960;
    final int screenHeight = 540;

    boolean loadBeam;

    Cannon(SpriteBatch batch, SoundPlayer sound) {
        this.batch = batch;
        this.sound = sound;

        cannon = new Texture("cannon.png");
        cannonFire = new Texture("cannon-fire.png");
        cannonBase = new Texture("cannon-base.png");
        beam = new Texture("beam.png");

        cannonSprite = new Sprite(cannon);
        cannonSprite.setPosition(450, 10);
        beamSprite = new Sprite(beam);

        beamPosition = new Vector2();
        beamAim = new Vector2();

        restart();
    }

    public void restart() {
        cannonRotation = 0;
        cannonOrientation = 1;//1 LEFT, -1 RIGHT

        beamPosition.x = 465;
        beamPosition.y = 85;
        beamAim.x = 0;
        beamAim.y = 0;

        loadBeam = true;

        fireStartTime = 0;
    }

    public void draw() {
        beamSprite.draw(batch);
        cannonSprite.draw(batch);
        batch.draw(cannonBase, 400, 10);
    }

    void rotate(float delta, Vector2 position) {

        if (pauseTime > 0) return;

        if (getElapsedTime() > 150) {
            cannonSprite.setTexture(cannonFire);
        }

        if (getElapsedTime() < 500) {
            return;
        }

        cannonSprite.setTexture(cannon);

        cannonRotation += cannonOrientation * delta * cannonSpeed;

        if (position.x > 450) {
            if (cannonRotation < -60) {
                cannonOrientation = 1;
            } else if (cannonRotation > 0) {
                cannonOrientation = -1;
            }
        } else {
            if (cannonRotation > 60) {
                cannonOrientation = -1;
            } else if (cannonRotation < 0) {
                cannonOrientation = 1;
            }
        }

        cannonSprite.setRotation(cannonRotation);
    }

    void fire(float delta) {

        if (pauseTime > 0) return;

        if (loadBeam == true) {
            if (getElapsedTime() > 1000) {
                fireStartTime = TimeUtils.millis();

                beamSprite.setAlpha(1.0f);

                //centro do canhão
                beamPosition.x = 465;
                beamPosition.y = 85;

                beamAim.x = 0;
                beamAim.y = 1;
                beamAim.rotate(cannonRotation);
                sound.playBeam();
                loadBeam = false;
            }
        } else {
            beamPosition.x += beamAim.x * delta * bulletSpeed;
            beamPosition.y += beamAim.y * delta * bulletSpeed;

            if ((beamPosition.x > screenWidth) || (beamPosition.x < 0) || (beamPosition.y > screenHeight) || (beamPosition.y < 0)) {
                loadBeam = true;
            } else {
                beamSprite.setPosition(beamPosition.x, beamPosition.y);
            }
        }
    }

    private long getElapsedTime() {
        if (pauseTime > 0) {
            return (pauseTime - fireStartTime);
        } else {
            return (TimeUtils.millis() - fireStartTime);
        }
    }

    public void pause() {
        pauseTime = TimeUtils.millis();
    }

    public void resume() {
        fireStartTime += TimeUtils.millis() - pauseTime;
        pauseTime = 0;
    }

    public void dispose() {
        cannon.dispose();
        cannonFire.dispose();
        cannonBase.dispose();
        beam.dispose();
    }

    public void setLevel(float level) {
        cannonSpeed *= level;
        bulletSpeed *= level;
    }

    public Vector2 getBeamPosition() {
        if (loadBeam)
            return null;
        return beamPosition;
    }

    public void setBeamAlpha(float beamAlpha) {
        beamSprite.setAlpha(beamAlpha);
        loadBeam = true;
    }
}
