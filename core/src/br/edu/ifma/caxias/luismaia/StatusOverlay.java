package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by luisfmss on 23/03/15.
 */
public class StatusOverlay implements Disposable {
    SpriteBatch batch;
    private Texture life1, life2, life3, bar, pause;
    private Sprite lifeSprite;
    private int lifeCount;
    private int score;
    private long scoreTimer;
    private BitmapFont font;
    private long pauseTime;


    StatusOverlay(SpriteBatch batch) {
        this.batch = batch;

        life1 = new Texture("life1.png");
        life2 = new Texture("life2.png");
        life3 = new Texture("life3.png");
        bar = new Texture("bar.png");
        pause = new Texture("pause.png");
        lifeSprite = new Sprite(life3);
        lifeSprite.setPosition(850, 500);

        initFont();
        restartGame();
    }

    public void draw() {

        batch.draw(bar, 0, 490);
        batch.draw(pause, 460, 495);

        updateLifeOverlay();
        updateScore();

        lifeSprite.draw(batch);
        font.draw(batch, formatScore(score), 10, 530);
    }

    void updateLifeOverlay() {

        switch (lifeCount) {
            case 1:
                lifeSprite.setTexture(life1);
                break;

            case 2:
                lifeSprite.setTexture(life2);
                break;

            case 3:
                lifeSprite.setTexture(life3);
                break;
        }
    }

    void updateScore() {
        if ((getElapsedTime() > 500)&&((pauseTime == 0))) {
            scoreTimer = TimeUtils.millis();
            ++score;
        }
    }

    public String formatScore(int score) {
        if (score < 10) {
            return "000" + String.valueOf(score);
        } else if (score < 100) {
            return "00" + String.valueOf(score);
        } else if (score < 1000) {
            return "0" + String.valueOf(score);
        }
        return String.valueOf(Math.min(score, 9999));
    }

    public void addScore(int add) {
        score += add;
    }

    private void initFont() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/exotic.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 47;
        parameter.borderColor = Color.valueOf("d43535");//red b10303
        parameter.color = Color.valueOf("f8fc3f");//yellow bbbd07
        parameter.borderWidth = 7;
        font = generator.generateFont(parameter);
        generator.dispose();
    }

    private long getElapsedTime() {
        if (pauseTime > 0) {
            return (pauseTime - scoreTimer);
        } else {
            return (TimeUtils.millis() - scoreTimer);
        }
    }

    public void pause() {
        pauseTime = TimeUtils.millis();
    }

    public void resume() {
        scoreTimer += TimeUtils.millis() - pauseTime;
        pauseTime = 0;
    }

    public int getLifeCount() {
        return lifeCount;
    }

    public void removeLife() {
        lifeCount = Math.max(lifeCount - 1, 0);
    }

    public void addLife() {
        lifeCount = Math.min(lifeCount + 1, 3);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void restartGame() {
        score = 0;
        lifeCount = 3;
        scoreTimer = TimeUtils.millis();
        pauseTime = 0;
    }

    public void dispose() {
        life1.dispose();
        life2.dispose();
        life3.dispose();
        bar.dispose();
        pause.dispose();
    }
}
