package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by luisfmss on 01/04/15.
 */
public class TexturedActor extends Actor implements Disposable {
    Texture texture;
    Sprite sprite;
    private float alpha;

    TexturedActor(String textureName) {
        texture = new Texture(textureName);
        sprite = new Sprite(texture);
        setBounds(getX(), getY(), texture.getWidth(), texture.getHeight());
    }

    @Override
    public void draw(Batch batch, float alpha) {
        sprite.setPosition(getX(), getY());
        sprite.setRotation(getRotation());
        sprite.setScale(getScaleX(), getScaleY());
        sprite.draw(batch, alpha);
    }


    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        sprite.setPosition(x, y);
    }

    @Override
    public void setRotation(float degrees) {
        super.setRotation(degrees);
        sprite.setRotation(degrees);
    }

    @Override
    public void setScale(float scaleXY) {
        super.setScale(scaleXY);
        sprite.setScale(scaleXY);
    }

    public void setTexture(String textureName) {
        texture.dispose();
        texture = new Texture(textureName);
        sprite.setTexture(texture);
    }

    public void dispose() {
        texture.dispose();
    }

    public void setAlpha(float alpha) {
        sprite.setAlpha(alpha);
    }
}
