package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

enum TYPE {
    SHIELD, STAR, LIFE, BOMB;
};

class ItemMapper {
    int cell;
    TYPE type;
    TexturedObject textureObj;

    ItemMapper(TexturedObject obj, TYPE type, int cell) {
        this.cell = cell;
        this.textureObj = obj;
        this.type = type;
    }

    void dispose() {
        textureObj.dispose();
        textureObj = null;
    }
}

/**
 * Created by luisfmss on 23/03/15.
 */
public class ItemPlacer implements Disposable {

    private final int minX = 30, minY = 70, numCol = 15, numRow = 7, cellSize = 60, TIME_TO_PLACE = 3000;

    List<ItemMapper> itens;
    List<Integer> cellPos;
    Vector2 position;
    SpriteBatch batch;
    long time;
    long pauseTime;

    ItemPlacer(SpriteBatch batch) {
        this.batch = batch;

        itens = new LinkedList<ItemMapper>();
        time = TimeUtils.millis();

        position = new Vector2();

        cellPos = new LinkedList<Integer>();
        for (int i = 0; i < numCol * numRow; i++) {
            cellPos.add(i);
        }

        //Cannon Position
        cellPos.remove(23);
        cellPos.remove(22);
        cellPos.remove(21);
        cellPos.remove(8);
        cellPos.remove(7);
        cellPos.remove(6);
    }

    public void restart() {
        Iterator<ItemMapper> it = itens.iterator();
        ItemMapper item;
        while (it.hasNext()) {
            item = it.next();
            cellPos.add(item.cell);
            item.dispose();
            it.remove();
        }

        time = TimeUtils.millis();
        pauseTime = 0;
    }

    public void placeItens() {

        if (getElapsedTime() < TIME_TO_PLACE) return;

        time = TimeUtils.millis();

        int rand = ((int) (Math.random() * 100)) % 20;

        //Gdx.app.log("rand", String.valueOf(rand));

        switch (rand) {
            case 0:
            case 1:
                addShield();
                break;

            case 2:
            case 3:
            case 4:
                addStar();
                break;

            case 5:
                addLife();
                break;

            case 6:
            case 7:
            case 8:
            case 9:
                addBomb();
                break;
        }

    }

    public void draw() {
        Iterator<ItemMapper> it = itens.iterator();
        ItemMapper item;
        while (it.hasNext()) {
            item = it.next();

            if (!item.textureObj.isTimeExpired()) {
                item.textureObj.draw();
            } else {
                cellPos.add(item.cell);
                item.dispose();
                it.remove();
            }
        }
    }

    void removeItem(ItemMapper item) {
        cellPos.add(item.cell);
        itens.remove(item);
        item.dispose();
    }

    int getEmptyCell() {

        if (cellPos.isEmpty()) return -1;

        int index = ((int) (Math.random() * numRow * numCol)) % (cellPos.size());

        int cell = cellPos.get(index);
        cellPos.remove(index);

//        Gdx.app.log("cell", String.valueOf(cell));
//        Gdx.app.log("size", String.valueOf(cellPos.size()));

        return cell;
    }

    void addShield() {
        TexturedObject shield = new TexturedObject(batch, "shield.png");

        int cell = getEmptyCell();

        if (cell == -1) return;

        shield.setPosition(getPositionFromCell(cell));

        itens.add(new ItemMapper(shield, TYPE.SHIELD, cell));
    }

    void addStar() {
        TexturedObject star = new TexturedObject(batch, "star.png");

        int cell = getEmptyCell();

        if (cell == -1) return;

        star.setPosition(getPositionFromCell(cell));

        itens.add(new ItemMapper(star, TYPE.STAR, cell));
    }

    void addLife() {
        TexturedObject life = new TexturedObject(batch, "life.png");

        int cell = getEmptyCell();

        if (cell == -1) return;

        life.setPosition(getPositionFromCell(cell));

        itens.add(new ItemMapper(life, TYPE.LIFE, cell));
    }

    void addBomb() {
        TexturedObject bomb = new TexturedObject(batch, "bomb.png");

        int cell = getEmptyCell();

        if (cell == -1) return;


        bomb.setPosition(getPositionFromCell(cell));

        itens.add(new ItemMapper(bomb, TYPE.BOMB, cell));
    }

    Vector2 getPositionFromCell(int cell) {
        int cellNum = (int) cell % numCol;
        position.x = minX + cellNum * cellSize;

        cellNum = (int) cell / numCol;
        position.y = minY + cellNum * cellSize;
        //Gdx.app.log("cell-x,y:", String.valueOf(x) + "-" + String.valueOf(y));
        return position;
    }

    public List<ItemMapper> getItens() {
        return itens;
    }

    public void dispose() {

    }

    private long getElapsedTime() {
        if (pauseTime > 0) {
            return (pauseTime - time);
        } else {
            return (TimeUtils.millis() - time);
        }
    }

    public void pause() {
        pauseTime = TimeUtils.millis();
        for (ItemMapper item : itens) {
            item.textureObj.pause();
        }

    }

    public void resume() {
        time += TimeUtils.millis() - pauseTime;
        pauseTime = 0;
        for (ItemMapper item : itens) {
            item.textureObj.resume();
        }
    }

}
