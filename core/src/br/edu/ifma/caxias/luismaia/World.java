package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by luisfmss on 30/03/15.
 */
public class World {

    GameScreen gameScreen;

    SpriteBatch batch;
    Texture background;
    ExplosionAnimation explosion;

    ItemPlacer itemPlacer;

    SpaceShip spaceShip;
    Cannon cannon;

    StatusOverlay statusOverlay;

    long startTime;

    long gameOverDelay;

    SoundPlayer sound;

    OrthographicCamera camera;
    Viewport viewport;

    List<ItemMapper> removeList;


    World(GameScreen gameScreen) {
        this.gameScreen = gameScreen;

        camera = new OrthographicCamera();
        viewport = new StretchViewport(960, 540, camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        batch = gameScreen.getSpriteBatch();
        sound = new SoundPlayer();
        sound.playBackground();

        background = new Texture("background.jpg");

        explosion = new ExplosionAnimation(batch);
        spaceShip = new SpaceShip(batch);
        cannon = new Cannon(batch, sound);
        itemPlacer = new ItemPlacer(batch);
        statusOverlay = new StatusOverlay(batch);
        removeList = new LinkedList<ItemMapper>();
        restartGame();
    }

    void restartGame() {

        spaceShip.restart();
        cannon.restart();

        startTime = TimeUtils.millis();

        statusOverlay.restartGame();
        itemPlacer.restart();

        setLevel(1);
    }

    public void render(float delta) {

        camera.update();

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, 0, 0);

        cannon.rotate(delta, spaceShip.getPosition());
        cannon.fire(delta);

        itemPlacer.draw();

        spaceShip.draw();
        cannon.draw();
        explosion.draw();
        statusOverlay.draw();

        batch.end();

        spaceShip.updatePosition(delta);

        detectCollision();

        itemPlacer.placeItens();
    }

    public void renderEnd(float delta) {

        camera.update();

        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, 0, 0);

        spaceShip.draw();
        cannon.draw();
        explosion.draw();

        batch.end();
    }

    void detectCollision() {

        Vector2 beamCore = cannon.getBeamPosition();

        if (beamCore != null) {

            Vector2 shipCore = spaceShip.getCenter();

            if (shipCore.dst2(beamCore) < spaceShip.radius() * spaceShip.radius()) {

                if (!spaceShip.isShieldedOrImmune()) {
                    statusOverlay.removeLife();
                    spaceShip.setLoseLife();
                    explosion.setPosition(beamCore);
                    cannon.setBeamAlpha(0.0f);
                    sound.playExplosion();
                }
            }
        }

        Iterator<ItemMapper> it = itemPlacer.getItens().iterator();
        removeList.clear();
        while (it.hasNext()) {
            ItemMapper item = it.next();

            if (item.textureObj.collides(spaceShip)) {
                switch (item.type) {
                    case SHIELD:
                        spaceShip.setBubbleShieldOn();
                        sound.playShield();
                        removeList.add(item);
                        break;
                    case STAR:
                        statusOverlay.addScore(100);
                        sound.playStar();
                        removeList.add(item);
                        break;
                    case LIFE:
                        statusOverlay.addLife();
                        sound.playLife();
                        removeList.add(item);
                        break;
                    case BOMB:
                        if (!spaceShip.isShieldedOrImmune()) {
                            statusOverlay.removeLife();
                            spaceShip.setLoseLife();
                            explosion.setPosition(item.textureObj.getCenter());
                            sound.playExplosion();
                            removeList.add(item);
                        }
                        break;
                }
            }
        }

        for (ItemMapper item : removeList) {
            itemPlacer.removeItem(item);
        }
        removeList.clear();

        if (statusOverlay.getLifeCount() <= 0) {
            gameScreen.endGame();
            gameOverDelay = TimeUtils.millis();
        }
    }

    void setLevel(float level) {
        spaceShip.setLevel(level);
        cannon.setLevel(level);
    }

    public void dispose() {
        batch.dispose();
        background.dispose();
        spaceShip.dispose();
        cannon.dispose();
        sound.dispose();
        statusOverlay.dispose();
        itemPlacer.dispose();
    }

    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
    }

    public int getScore() {
        return statusOverlay.getScore();
    }

    public void setTouching(boolean touching) {
        if (touching) {
            spaceShip.setEngineOn();
//            sound.playEngine();
        } else {
            spaceShip.setEngineOff();
//            sound.pauseEngine();
        }
    }

    public void pause() {
        statusOverlay.pause();
        spaceShip.pause();
        itemPlacer.pause();
        cannon.pause();
        explosion.pause();
    }

    public void resume() {
        statusOverlay.resume();
        spaceShip.resume();
        itemPlacer.resume();
        cannon.resume();
        explosion.resume();
    }

    public void toggleMute() {
    }
}
