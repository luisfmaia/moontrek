package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.TimeUtils;


/**
 * Created by luisfmss on 30/03/15.
 */
public class GameScreen implements Screen {

    enum GameState {
        PLAYING,
        PAUSED,
        GAMEOVER
    }

    private static final float INPUT_TIME = 1500;
    private MoonTrek game;
    private World world;
    private Texture gameEnd;
    private Texture pauseMenu;
    private Sprite spriteEnd;
    private SpriteBatch batch;

    private long inputDelay;
    private boolean touched;
    private boolean paused;

    private GameState state;//0 PLAY, 1 GAME OVER, 2 PAUSE

    GameScreen(MoonTrek game) {
        this.game = game;
        state = GameState.PLAYING;
        inputDelay = 0;
        batch = new SpriteBatch();
    }

    @Override
    public void show() {
        world = new World(this);
        Gdx.input.setCatchBackKey(true);
        touched = false;
        paused = false;
        pauseMenu = new Texture("pause-menu.png");

//        com.badlogic.gdx.utils.Timer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
//            @Override
//            public void run() {
//                Gdx.app.log("Java:", String.valueOf(Gdx.app.getJavaHeap()));
//                Gdx.app.log("Native:", String.valueOf(Gdx.app.getNativeHeap()));
//            }
//        }
//                , 0, 5);

//        com.badlofloatgic.gdx.utils.Timer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
//            @Override
//            public void run() {
//                Gdx.app.log("Java:", String.valueOf(Gdx.app.getJavaHeap()));
//                Gdx.app.log("Native:", String.valueOf(Gdx.app.getNativeHeap()));
//                Gdx.app.log("GC:", "GC: on the road");
//                System.gc();
//                Gdx.app.log("Java:", String.valueOf(Gdx.app.getJavaHeap()));
//                Gdx.app.log("Native:", String.valueOf(Gdx.app.getNativeHeap()));
//            }
//        }
//                , 30, 30);

    }


    @Override
    public void render(float delta) {

        if (state == GameState.GAMEOVER) {
            long timeDiff = TimeUtils.millis() - inputDelay;

            if (timeDiff < INPUT_TIME) {
                spriteEnd.setAlpha(timeDiff / INPUT_TIME);
                spriteEnd.setScale(10 - (timeDiff / INPUT_TIME) * 9);
            } else {
                spriteEnd.setAlpha(1f);
                spriteEnd.setScale(1f);
            }

            world.renderEnd(delta);
            batch.begin();
            spriteEnd.draw(batch);
            batch.end();


            if (Gdx.input.isTouched()) {
                //Add a button here
                if (timeDiff > INPUT_TIME) {
                    world.restartGame();
                    state = GameState.PAUSED;
                    world.pause();
                    inputDelay = TimeUtils.millis();
                }
            }
            return;
        }


        world.render(delta);
        processInput();

        if (state == GameState.PAUSED) {
            batch.begin();
            batch.draw(pauseMenu, 330, 100);
            batch.end();
        }
    }

    void processInput() {

        if (Gdx.input.isTouched()) {

            if ((Gdx.input.getX() > 460) && (Gdx.input.getX() < 500) && (Gdx.input.getY() < 50)) {

                if (TimeUtils.millis() - inputDelay < 500) return;

                if (state == GameState.PAUSED) {
                    world.resume();
                    state = GameState.PLAYING;
                } else {
                    state = GameState.PAUSED;
                    world.pause();
                }

                Gdx.app.log("pausedButton", String.valueOf(paused));
                inputDelay = TimeUtils.millis();
                return;
            }

            if (state == GameState.PLAYING) {
                if (touched == false) {
                    world.setTouching(true);
                }
                touched = true;
                return;
            }

            if (state == GameState.PAUSED) {
//                Gdx.app.log("X", String.valueOf(Gdx.input.getX()));
//                Gdx.app.log("Y", String.valueOf(Gdx.input.getY()));
                if ((Gdx.input.getX() > 345) && (Gdx.input.getX() < 620)) {

                    if ((Gdx.input.getY() > 175) && (Gdx.input.getY() < 225)) {
                        Gdx.app.log("pause", "play");
                        world.resume();
                        state = GameState.PLAYING;
                        return;
                    }

                    if ((Gdx.input.getY() > 270) && (Gdx.input.getY() < 320)) {
                        Gdx.app.log("pause", "mute");
                        world.toggleMute();
                        return;
                    }

                    if ((Gdx.input.getY() > 345) && (Gdx.input.getY() < 395)) {
                        Gdx.app.log("pause", "quit");
                        backToMenu();
                        return;
                    }

                }
            }

//            Vector3 coord = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
//            if (coord.x > screenWidth / 2) {
//                spaceShip.setRotation(-90);
//                orientation = 0;//move right
//            } else {
//                spaceShip.setRotation(90);
//                orientation = 1;//move left
//            }

//            Gdx.app.log("GDXInput:", new Vector2(Gdx.input.getX(), Gdx.input.getY()).toString());
//            if ((coord.y > 500) && (coord.x > 450 && coord.x < 510)) {
//                Gdx.app.log("coord:",coord.toString());
//                //state = 2;//PAUSE
//                return;
//            }

        } else {
            if (state == GameState.PLAYING) {
                if (touched == true) {
                    world.setTouching(false);
                }
                touched = false;
            }

        }

    }

    private void backToMenu() {
        game.setScreen(new MenuScreen(game));
    }

    @Override
    public void resize(int width, int height) {
        world.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setCatchBackKey(false);
        world.dispose();
    }

    @Override
    public void dispose() {

    }

    public void endGame() {
        int scoreRecord = 0;
        Preferences prefs = Gdx.app.getPreferences("MoonTrek");
        if (prefs.contains("record")) {
            scoreRecord = prefs.getInteger("record");
        }

        if (gameEnd != null) {
            gameEnd.dispose();
        }

        if (scoreRecord < world.getScore()) {
            scoreRecord = world.getScore();
            prefs.putInteger("record", scoreRecord);
            prefs.flush();
            gameEnd = new Texture("first.png");

        } else {
            gameEnd = new Texture("gameover.png");
        }

        inputDelay = TimeUtils.millis();
        state = GameState.GAMEOVER;

        spriteEnd = new Sprite(gameEnd);
        spriteEnd.setPosition(330, 120);
        spriteEnd.setScale(10f);
        spriteEnd.setAlpha(0f);
    }

    public SpriteBatch getSpriteBatch() {
        return batch;
    }
}
