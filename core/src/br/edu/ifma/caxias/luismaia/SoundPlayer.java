package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by luisfmss on 26/03/15.
 */
public class SoundPlayer implements Disposable {
//    public static SoundPlayer instance = null;

    Music background;
//    Music backgroundMenu;
//    Sound engine;
    Sound star;
    Sound shield;
    Sound explosion;
    Sound life;
    Sound beam;
//    long engineId = 0;

    public SoundPlayer() {
        background = Gdx.audio.newMusic(Gdx.files.internal(("sounds/background.mp3")));
        background.setLooping(true);
        background.setVolume(0.3f);

//        backgroundMenu = Gdx.audio.newMusic(Gdx.files.internal(("sounds/background-menu.mp3")));
//        backgroundMenu.setLooping(true);
//        backgroundMenu.setVolume(0.7f);

//        engine = Gdx.audio.newSound(Gdx.files.internal("sounds/engine.wav"));
//        engineId = engine.play(0);
//        engine.setLooping(engineId, true);
//        engine.pause(engineId);
//        engine.setVolume(engineId, 0.2f);

        star = Gdx.audio.newSound(Gdx.files.internal("sounds/star.wav"));
        shield = Gdx.audio.newSound(Gdx.files.internal("sounds/shield.wav"));
        explosion = Gdx.audio.newSound(Gdx.files.internal("sounds/explosion.wav"));
        life = Gdx.audio.newSound(Gdx.files.internal("sounds/life.wav"));
        beam = Gdx.audio.newSound(Gdx.files.internal("sounds/beam.wav"));

    }

    public void playBackground() {

//        if (backgroundMenu.isPlaying()) {
//            backgroundMenu.stop();
//        }

        if (!background.isPlaying()) {
            background.play();
        }
    }

//    public void playBackgroundMenu() {
//
//        if (background.isPlaying()) {
//            background.stop();
//        }
//
//        if (!backgroundMenu.isPlaying()) {
//            backgroundMenu.play();
//        }
//    }

//    public void playEngine() {
//        engine.resume(engineId);
//    }
//
//    public void pauseEngine() {
//        engine.pause(engineId);
//    }

    public void playStar() {
        long id = star.play();
        star.setVolume(id, 0.7f);
    }

    public void playShield() {
        long id = shield.play();
        shield.setVolume(id, 0.7f);
    }

    public void playExplosion() {
        long id = explosion.play();
        explosion.setVolume(id, 1.0f);
    }

    public void playLife() {
        long id = life.play();
        life.setVolume(id, 0.7f);
    }

    public void playBeam() {
        long id = beam.play();
        beam.setVolume(id, 0.5f);
    }

//    public static SoundPlayer getInstance() {
//        if (instance == null) {
//            instance = new SoundPlayer();
//        }
//        return instance;
//    }

    public void dispose() {
        background.dispose();
        star.dispose();
        shield.dispose();
        explosion.dispose();
        life.dispose();
        beam.dispose();
    }
}
