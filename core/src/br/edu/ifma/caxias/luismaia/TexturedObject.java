package br.edu.ifma.caxias.luismaia;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by luisfmss on 23/03/15.
 */
public class TexturedObject implements CollisionObject, Disposable {
    private Texture texture;
    private Sprite sprite;
    private SpriteBatch batch;
    private float rad;
    private boolean visible;
    private long time;
    private final long START_TIME = 1000;
    private final long SHOW_TIME = 7000;
    private final long END_TIME = 10000;
    private Vector2 position;
    private Vector2 center;
    private long pauseTime;

    public TexturedObject(SpriteBatch batch, String name) {
        this.batch = batch;
        texture = new Texture(name);
        sprite = new Sprite(texture);
        visible = true;
        setRadiusMax();
        position = new Vector2();
        center = new Vector2();

        restart();
    }

    public void restart() {
        time = TimeUtils.millis();
        pauseTime = 0;
    }

    public void setPosition(Vector2 position) {
        sprite.setPosition(position.x, position.y);
    }

    public void setPosition(float x, float y) {
        sprite.setPosition(x, y);
    }

    public void draw() {
        long timeDiff = getElapsedTime();
        if ((timeDiff < START_TIME) || (timeDiff > SHOW_TIME)) {
            sprite.setAlpha((timeDiff / 250) % 2);
        }

        if (visible == true) {
            sprite.draw(batch);
        }
    }

    public boolean isTimeExpired() {
        return (getElapsedTime() > END_TIME);
    }

    private boolean wasJustPlaced() {
        return (getElapsedTime() < START_TIME);
    }

    private long getElapsedTime() {
        if (pauseTime > 0) {
            return (pauseTime - time);
        } else {
            return (TimeUtils.millis() - time);
        }
    }

    public void pause() {
        pauseTime = TimeUtils.millis();
    }

    public void resume() {
        time += TimeUtils.millis() - pauseTime;
        pauseTime = 0;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;

    }

    public void setRadiusMax() {
        rad = Math.max(sprite.getWidth(), sprite.getHeight());
    }

    public void setRadiusMin() {
        rad = Math.min(sprite.getWidth(), sprite.getHeight());
    }

    public Vector2 getPosition() {
        position.x = sprite.getX();
        position.y = sprite.getY();
        return position;
    }

    public float radius() {
        return rad;
    }

    public Vector2 getCenter() {
        sprite.getBoundingRectangle().getCenter(center);
        return center;
    }

    public boolean collides(CollisionObject obj) {
        if ((visible == false) || wasJustPlaced())
            return false;

        Vector2 center = getCenter();
        Vector2 objCenter = obj.getCenter();
        if (center.dst2(objCenter) < obj.radius() * this.radius())
            return true;
        else
            return false;
    }

    public void dispose() {
//        sprite = null;
        texture.dispose();
//        texture = null;
    }

    public Sprite getSprite() {
        return sprite;
    }
}